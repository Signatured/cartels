package me.signatured.cartel;

import io.puharesource.mc.titlemanager.api.ActionbarTitleObject;

import org.bukkit.entity.Player;

public class ActionbarHandler {
	
	private static ActionbarHandler instance;
	
	public ActionbarHandler() {
		instance = this;
	}
	
	public static ActionbarHandler getInstance() {
		return instance;
	}
	
	public void sendActionbarMessage(Player player, String message) {
		new ActionbarTitleObject(message).send(player);
	}
	
	public void sendActionbarBroadcast(Player player, String message) {
		new ActionbarTitleObject(message).broadcast();
	}
	
	public void clearActionbar(Player player) {
		new ActionbarTitleObject(" ").send(player);
	}
}

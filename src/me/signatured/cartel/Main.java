package me.signatured.cartel;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;

public class Main extends JavaPlugin {
	
	private static Main instance;
	
	public Main() {
		instance = this;
	}
	
	public static Main getInstance() {
		return instance;
	}
	
	@Override
	@SuppressWarnings("deprecation")
	public void onEnable() {
		
		if (getWorldGuard() == null || getWorldEdit() == null) {
			this.setEnabled(false);
		} else if (getServer().getPluginManager().getPlugin("TitleManager") == null || !getServer().getPluginManager().getPlugin("TitleManager").isEnabled()) {
			getLogger().warning("Failed to hook into TitleManager, disabling plugin!");
			getPluginLoader().disablePlugin(this);
		} else if (!Bukkit.getPluginManager().isPluginEnabled("HolographicDisplays")) {
	        getLogger().severe("*** HolographicDisplays is not installed or not enabled. ***");
	        getLogger().severe("*** This plugin will be disabled. ***");
	        this.setEnabled(false);
	        return;
	    }
		
		loadConfig();
		CartelManager.getManager().loadCartels();
		getCommand("setcartelholo").setExecutor(new CommandManager());
		Bukkit.getScheduler().runTaskTimer(this, new CartelTask(), 20L, 20L);
		
		new TitleHandler();
		new ActionbarHandler();
	}
	
	public void onDisable() {
		for (Cartel cartel : Cartel.cartelObjects) {
			Hologram holo = cartel.getHolo();
			if (holo != null) {
				holo.delete();
			}
		}
	}
	
	public WorldGuardPlugin getWorldGuard() {
        Plugin plugin = Bukkit.getServer().getPluginManager().getPlugin("WorldGuard");
     
        if (plugin == null || !(plugin instanceof WorldGuardPlugin)) {
            return null;
        }
     
        return (WorldGuardPlugin) plugin;
    }
	
	public WorldEditPlugin getWorldEdit() {
        Plugin plugin = Bukkit.getServer().getPluginManager().getPlugin("WorldEdit");
         
        if (plugin == null || !(plugin instanceof WorldEditPlugin)) {
        	return null;
        }
        
        return (WorldEditPlugin) plugin;
	}
	
	public void loadConfig() {
		List<String> list = getConfig().getStringList("Example");
		list.add("drug1");
		list.add("drug2");
		list.add("drug3");
		list.add("drug4");
		list.add("drug5");
		getConfig().set("cartels", list);
		getConfig().options().copyDefaults(true);
		saveConfig();
	}
}

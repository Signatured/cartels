package me.signatured.cartel;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;

public class CommandManager implements CommandExecutor {
	
	FileConfiguration config = Main.getInstance().getConfig();

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if (cmd.getName().equalsIgnoreCase("setcartelholo")) {
			if (sender instanceof Player) {
				Player player = (Player) sender;
				
				if (player.hasPermission("cartel.setholo")) {
					if (args.length == 1) {
						for (Cartel cartel : Cartel.cartelObjects) {
							if (cartel.getName().equalsIgnoreCase(args[0])) {
								Location holoLoc = player.getLocation().add(0, 3, 0);
								Hologram hologram = HologramsAPI.createHologram(Main.getInstance(), holoLoc);
								
								cartel.setHolo(hologram);
								saveHolo(cartel, holoLoc);
								
								player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aHologram for " + args[0] + " set!"));
							}
						}
					} else player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cUsage: /setcartelholo <name>"));
				} else player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cYou don't have permission to do this!"));
			}
		}
		return true;
	}
	
	public void saveHolo(Cartel cartel, Location loc) {
		String path = "holograms." + cartel.getName();
		
		config.set(path, CartelManager.getManager().serializeLoc(loc));
		Main.getInstance().saveConfig();
	}
}

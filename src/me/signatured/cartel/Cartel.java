package me.signatured.cartel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import net.gizmoservers.teams.util.Team;

import com.gmail.filoghost.holographicdisplays.api.Hologram;

public class Cartel {
	
	public enum Type {
		DRUG1, DRUG2, DRUG3, DRUG4, DRUG5;
	}
	
	public static ArrayList<Cartel> cartelObjects = new ArrayList<Cartel>();
	
	private String name;
	private String status;
	
	private Type type;
	private Team team;
	private Team teamTakingOver;
	
	private Hologram holo;
	
	private HashMap<Team, Integer> teams = new HashMap<Team, Integer>();
	
	private boolean beingTakenOver;
	
	public Cartel(String cartelName, Type type) {
		this.name = cartelName;
		this.type = type;
		this.holo = null;
		this.team = null;
		this.status = "";
		this.beingTakenOver = false;
		
		cartelObjects.add(this);
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public boolean isNeutral() {
		if (this.team == null) {
			return true;
		}
		return false;
	}
	
	public Team getTeam() {
		return this.team;
	}
	
	public void setTeam(Team team) {
		this.team = team;
	}
	
	public Hologram getHolo() {
		return this.holo;
	}
	
	public void setHolo(Hologram holo) {
		this.holo = holo;
	}
	
	public String getStatus() {
		return this.status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public Type getType() {
		return this.type;
	}
	
	public void setType(Type type) {
		this.type = type;
	}
	
	public boolean isBeingTakenOver() {
		return this.beingTakenOver;
	}
	
	public Team getTeamTakingOver() {
		if (isBeingTakenOver()) {
			return this.teamTakingOver;
		}
		return null;
	}
	
	public void setBeingTakenOver(boolean b, Team team) {
		this.beingTakenOver = b;
		this.teamTakingOver = team;
	}
	
	public HashMap<Team, Integer> getTeamsAndCount() {
		return this.teams;
	}
	
	public Team getTopPlayerTeam() {
		Team team = null;
		int topplayercount = 0;
		boolean tied = false;
		
		for (Entry<Team, Integer> entry : teams.entrySet()) {
			Team t = entry.getKey();
			
			if (entry.getValue() > topplayercount) {
				topplayercount = entry.getValue();
				team = t;
			} else if (entry.getValue() == topplayercount) {
				System.out.println("count = current top players");
				int tiedNum = entry.getValue();
				boolean higher = false;
				
				for (int num : getTeamsAndCount().values()) {
					if (num > tiedNum) higher = true;
					
					if (!higher) {
						System.out.println("TIED");
						return null;
					}
				}
			}
		}
		if (tied) {
			System.out.println("TIED = TRUE");
			return null;
		}
		return team;
	}
}

package me.signatured.cartel;

import net.gizmoservers.teams.util.Team;
import net.gizmoservers.teams.util.TeamMember;
import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;

public class CartelManager {
	
	private static CartelManager cm = new CartelManager();
	
	public static CartelManager getManager() {
		return cm;
	}
	
	public Cartel getCartel(String cartelName) {
		for (Cartel c : Cartel.cartelObjects) {
			if (c.getName().equals(cartelName)) {
				return c;
			}
		}
		return null;
	}
	
	public void setTeam(String cartelName, Team team) {
		if (getCartel(cartelName) != null) {
			Cartel cartel = getCartel(cartelName);
			
			cartel.setTeam(team);
		}
	}
	
	public void addTeamAndCount(String cartelName, Team team, int count) {
		if (getCartel(cartelName) != null) {
			Cartel cartel = getCartel(cartelName);
			
			cartel.getTeamsAndCount().put(team, count);
		}
	}
	
	public void updateHolo(String cartelName) {
		if (getCartel(cartelName) != null) {
			Cartel cartel = getCartel(cartelName);
			
			if (cartel.getHolo() != null) {
				Hologram holo = cartel.getHolo();				
				
				if (cartel.isNeutral() && !cartel.isBeingTakenOver()) {
					if (!cartel.getStatus().contains("Neutral")) {
						cartel.setStatus(ChatColor.translateAlternateColorCodes('&', "&6Neutral"));
						if (holo.size() >= 1) {
							holo.removeLine(0);
							holo.appendTextLine(ChatColor.translateAlternateColorCodes('&', "&bStatus: " + cartel.getStatus()));
						} else holo.appendTextLine(ChatColor.translateAlternateColorCodes('&', "&bStatus: " + cartel.getStatus()));
					}
				} else if (cartel.isBeingTakenOver()) {
					if (!cartel.getStatus().contains("Captured")) {
						cartel.setStatus(ChatColor.translateAlternateColorCodes('&', "&6Being Captured (" + cartel.getTeamTakingOver().getName() + ")"));
						if (holo.size() >= 1) {
							holo.removeLine(0);
							holo.appendTextLine(ChatColor.translateAlternateColorCodes('&', "&bStatus: " + cartel.getStatus()));
						} else holo.appendTextLine(ChatColor.translateAlternateColorCodes('&', "&bStatus: " + cartel.getStatus()));
					}
				} else if (cartel.getTeam() != null) {
					if (!cartel.getStatus().contains("Controlled")) {
						cartel.setStatus(ChatColor.translateAlternateColorCodes('&', "&6Controlled (" + cartel.getTeam().getName() + ")"));
						if (holo.size() >= 1) {
							holo.removeLine(0);
							holo.appendTextLine(ChatColor.translateAlternateColorCodes('&', "&bStatus: " + cartel.getStatus()));
						} else holo.appendTextLine(ChatColor.translateAlternateColorCodes('&', "&bStatus: " + cartel.getStatus()));
					}
				}
			}
		}
	}
	
	public boolean isNeutral(String cartelName) {
		if (getCartel(cartelName) != null) {
			Cartel cartel = getCartel(cartelName);
			
			if (cartel.isNeutral()) {
				return true;
			}
		}
		return false;
	}
	
	public void takingOverProcess(String cartelName, Team team) {
		if (getCartel(cartelName) != null) {
			Cartel cartel = getCartel(cartelName);
			
			if (team != cartel.getTeam()) {
				cartel.setBeingTakenOver(true, team);
				
				if (isNeutral(cartel.getName())) {
					new BukkitRunnable() {
						int countdown = 10;
						@Override
						public void run() {
							if (cartel.getTeamTakingOver() != team || cartel.getTeamTakingOver() == null || !cartel.isBeingTakenOver()) {
								sendCancelMessage(team, cartel);
								cartel.setBeingTakenOver(false, null);
								
								this.cancel();
								return;
							} else if (countdown <= 0) {
								sendSuccessTakeoverMessage(team, cartel);
								
								cartel.setTeam(team);
								cartel.setBeingTakenOver(false, null);
								
								this.cancel();
								return;
							} else sendTakeoverCountdownMessage(team, cartel, countdown);
							
							countdown--;
						}
					}.runTaskTimer(Main.getInstance(), 0L, 20L);
				} else {
					new BukkitRunnable() {
						int countdown = 10;
						@Override
						public void run() {
							if (cartel.getTeamTakingOver() != team || cartel.getTeamTakingOver() == null || !cartel.isBeingTakenOver()) {
								sendCancelMessage(team, cartel);
								cartel.setBeingTakenOver(false, null);
								
								this.cancel();
								return;
							} else if (countdown <= 0) {
								sendSuccessNeutralMessage(team, cartel);
								
								cartel.setTeam(null);
								cartel.setBeingTakenOver(false, null);
								
								this.cancel();
								return;
							} else sendNeutralCountdownMessage(team, cartel, countdown);
							
							countdown--;
						}
					}.runTaskTimer(Main.getInstance(), 0L, 20L);
				}
			}
		}
	}
	
	public void cancelTakingOver(Cartel cartel) {
		cartel.setBeingTakenOver(false, null);
	}
	
	public void sendCancelMessage(Team team, Cartel cartel) {
		for (Player player : Bukkit.getOnlinePlayers()) {
			TeamMember member = TeamMember.get(player.getUniqueId());
			
			if (member != null) {
				if (member.getTeam() == team && CartelTask.getManager().isInCartel(player.getLocation())) {
					ActionbarHandler.getInstance().sendActionbarMessage(player, ChatColor.translateAlternateColorCodes('&', "&cTaking over " + cartel.getName() + " cancelled!"));
				}
			}
		}
	}
	
	public void sendSuccessTakeoverMessage(Team team, Cartel cartel) {
		for (Player player : Bukkit.getOnlinePlayers()) {
			TeamMember member = TeamMember.get(player.getUniqueId());
			
			if (member != null) {
				if (member.getTeam() == team && CartelTask.getManager().isInCartel(player.getLocation())) {
					TitleHandler.getInstance().clearTitleAndSubtitle(player);
					TitleHandler.getInstance().sendSubtitleNoFade(player, ChatColor.translateAlternateColorCodes('&', "&6Took over &8" + cartel.getName() + " &6cartel!"));
				}
			}
		}
	}
	
	public void sendSuccessNeutralMessage(Team team, Cartel cartel) {
		for (Player player : Bukkit.getOnlinePlayers()) {
			TeamMember member = TeamMember.get(player.getUniqueId());
			
			if (member != null) {
				if (member.getTeam() == team && CartelTask.getManager().isInCartel(player.getLocation())) {
					TitleHandler.getInstance().clearTitleAndSubtitle(player);
					TitleHandler.getInstance().sendSubtitleNoFade(player, ChatColor.translateAlternateColorCodes('&', "&8" + cartel.getName() + " &6is now neutral!"));
				}
			}
		}
	}
	
	public void sendTakeoverCountdownMessage(Team team, Cartel cartel, int countdown) {
		for (Player player : Bukkit.getOnlinePlayers()) {
			TeamMember member = TeamMember.get(player.getUniqueId());
			
			if (member != null) {
				if (member.getTeam() == team && CartelTask.getManager().isInCartel(player.getLocation())) {
					ActionbarHandler.getInstance().sendActionbarMessage(player, ChatColor.translateAlternateColorCodes('&', "&6Taking over &8" + cartel.getName() + "&6: &7" + countdown));
				}
			}
		}
	}
	
	public void sendNeutralCountdownMessage(Team team, Cartel cartel, int countdown) {
		for (Player player : Bukkit.getOnlinePlayers()) {
			TeamMember member = TeamMember.get(player.getUniqueId());
			
			if (member != null) {
				if (member.getTeam() == team && CartelTask.getManager().isInCartel(player.getLocation())) {
					ActionbarHandler.getInstance().sendActionbarMessage(player, ChatColor.translateAlternateColorCodes('&', "&6Making &8" + cartel.getName() + " &6neutral&6: &7" + countdown));
				}
			}
		}
	}
	
	public String serializeLoc(Location l){
		return l.getWorld().getName() + "," + l.getBlockX() + "," + l.getBlockY() + "," + l.getBlockZ();
	}
 
	public Location deserializeLoc(String s){
		String[] st = s.split(",");
		return new Location(Bukkit.getWorld(st[0]), Integer.parseInt(st[1]), Integer.parseInt(st[2]), Integer.parseInt(st[3]));
	}
	
	@SuppressWarnings("unused")
	public void createCartel(String cartelName, Cartel.Type type) {
		Cartel cartel = new Cartel(cartelName, type);
	}
	
	@SuppressWarnings("unused")
	public void loadCartels() {
		Cartel drug1 = new Cartel("drug1", Cartel.Type.DRUG1);
		Cartel drug2 = new Cartel("drug2", Cartel.Type.DRUG2);
		Cartel drug3 = new Cartel("drug3", Cartel.Type.DRUG3);
		Cartel drug4 = new Cartel("drug4", Cartel.Type.DRUG4);
		Cartel drug5 = new Cartel("drug5", Cartel.Type.DRUG5);
		
		FileConfiguration fc = Main.getInstance().getConfig();
		ConfigurationSection cs = fc.getConfigurationSection("holograms");
		
		if (fc != null && cs != null) {
			for (String cartelName : cs.getKeys(false)) {
				if (cartelName != null) {
					String path = "holograms." + cartelName;
					Location loc = deserializeLoc(fc.getString(path));
					
					if (getCartel(cartelName) != null) {
						Cartel cartel = getCartel(cartelName);
						Hologram hologram = HologramsAPI.createHologram(Main.getInstance(), loc);
						
						cartel.setHolo(hologram);
					}
				}
			}
		}
	}
}

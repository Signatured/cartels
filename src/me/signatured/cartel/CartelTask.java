package me.signatured.cartel;

import java.util.ArrayList;
import java.util.List;

import net.gizmoservers.teams.util.Team;
import net.gizmoservers.teams.util.TeamMember;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class CartelTask extends BukkitRunnable {
	
	WorldGuardPlugin worldGuard = Main.getInstance().getWorldGuard();
	CartelManager cm = CartelManager.getManager();
	
	private static CartelTask ct = new CartelTask();
	
	public static CartelTask getManager() {
		return ct;
	}
	
	private Cartel cartel1 = CartelManager.getManager().getCartel("drug1");
	private Cartel cartel2 = CartelManager.getManager().getCartel("drug2");
	private Cartel cartel3 = CartelManager.getManager().getCartel("drug3");
	private Cartel cartel4 = CartelManager.getManager().getCartel("drug4");
	private Cartel cartel5 = CartelManager.getManager().getCartel("drug5");
	
	private ArrayList<String> cartel1Players = new ArrayList<String>();
	private ArrayList<String> cartel2Players = new ArrayList<String>();
	private ArrayList<String> cartel3Players = new ArrayList<String>();
	private ArrayList<String> cartel4Players = new ArrayList<String>();
	private ArrayList<String> cartel5Players = new ArrayList<String>();
	
	@Override
	public void run() {
		for (Player player : Bukkit.getOnlinePlayers()) {
			TeamMember member = TeamMember.get(player.getUniqueId());
			
			if (member != null && member.getTeam() != null && isInCartel(player.getLocation())) {
				String cartelName = getCartel(player.getLocation());
				Team team = member.getTeam();
				Cartel cartel = CartelManager.getManager().getCartel(cartelName);
				
				switch (cartelName) {
				
				case "drug1":
					if (!cartel.getTeamsAndCount().keySet().contains(team)) {
						cartel.getTeamsAndCount().put(team, 1);
						cartel1Players.add(player.getName());
					} else {
						if (!cartel1Players.contains(player.getName())) {
							int current = cartel.getTeamsAndCount().get(team);
							
							current++;
							cartel.getTeamsAndCount().put(team, current);
						}
					}
					break;
				
				case "drug2":					
					if (!cartel.getTeamsAndCount().keySet().contains(team)) {
						cartel.getTeamsAndCount().put(team, 1);
						cartel2Players.add(player.getName());
					} else {
						if (!cartel2Players.contains(player.getName())) {
							int current = cartel.getTeamsAndCount().get(team);
							
							current++;
							cartel.getTeamsAndCount().put(team, current);
						}
					}
					break;
					
				case "drug3":					
					if (!cartel.getTeamsAndCount().keySet().contains(team)) {
						cartel.getTeamsAndCount().put(team, 1);
						cartel3Players.add(player.getName());
					} else {
						if (!cartel3Players.contains(player.getName())) {
							int current = cartel.getTeamsAndCount().get(team);
							
							current++;
							cartel.getTeamsAndCount().put(team, current);
						}
					}
					break;
					
				case "drug4":					
					if (!cartel.getTeamsAndCount().keySet().contains(team)) {
						cartel.getTeamsAndCount().put(team, 1);
						cartel4Players.add(player.getName());
					} else {
						if (!cartel4Players.contains(player.getName())) {
							int current = cartel.getTeamsAndCount().get(team);
							
							current++;
							cartel.getTeamsAndCount().put(team, current);
						}
					}
					break;
					
				case "drug5":					
					if (!cartel.getTeamsAndCount().keySet().contains(team)) {
						cartel.getTeamsAndCount().put(team, 1);
						cartel5Players.add(player.getName());
					} else {
						if (!cartel5Players.contains(player.getName())) {
							int current = cartel.getTeamsAndCount().get(team);
							
							current++;
							cartel.getTeamsAndCount().put(team, current);
						}
					}
					break;
				}
			}
		}
		
		if (cartel1 != null) {
			Team topTeam = cartel1.getTopPlayerTeam();
			
			if (topTeam != null) {
				if (cartel1.isBeingTakenOver()) {
					if (topTeam != cartel1.getTeamTakingOver()) {
						if (topTeam != cartel1.getTeam()) {
							CartelManager.getManager().takingOverProcess(cartel1.getName(), topTeam);
						} else {
							cartel1.setBeingTakenOver(false, null);
						}
					}
				} else {
					if (topTeam != cartel1.getTeam()) {
						CartelManager.getManager().takingOverProcess(cartel1.getName(), topTeam);
					}
				}
			} else {
				if (cartel1.isBeingTakenOver()) {
					CartelManager.getManager().cancelTakingOver(cartel1);
				}
			}
		}
		
		if (cartel2 != null) {
			Team topTeam = cartel2.getTopPlayerTeam();
			
			if (topTeam != null) {
				if (cartel2.isBeingTakenOver()) {
					if (topTeam != cartel2.getTeamTakingOver()) {
						if (topTeam != cartel2.getTeam()) {
							CartelManager.getManager().takingOverProcess(cartel2.getName(), topTeam);
						} else {
							cartel2.setBeingTakenOver(false, null);
						}
					}
				} else {
					if (topTeam != cartel2.getTeam()) {
						CartelManager.getManager().takingOverProcess(cartel2.getName(), topTeam);
					}
				}
			}
		} else {
			if (cartel1.isBeingTakenOver()) {
				CartelManager.getManager().cancelTakingOver(cartel1);
			}
		}
		
		if (cartel3 != null) {
			Team topTeam = cartel3.getTopPlayerTeam();
			
			if (topTeam != null) {
				if (cartel3.isBeingTakenOver()) {
					if (topTeam != cartel3.getTeamTakingOver()) {
						if (topTeam != cartel3.getTeam()) {
							CartelManager.getManager().takingOverProcess(cartel3.getName(), topTeam);
						} else {
							cartel3.setBeingTakenOver(false, null);
						}
					}
				} else {
					if (topTeam != cartel3.getTeam()) {
						CartelManager.getManager().takingOverProcess(cartel3.getName(), topTeam);
					}
				}
			}
		} else {
			if (cartel1.isBeingTakenOver()) {
				CartelManager.getManager().cancelTakingOver(cartel1);
			}
		}
		
		if (cartel4 != null) {
			Team topTeam = cartel4.getTopPlayerTeam();
			
			if (topTeam != null) {
				if (cartel4.isBeingTakenOver()) {
					if (topTeam != cartel4.getTeamTakingOver()) {
						if (topTeam != cartel4.getTeam()) {
							CartelManager.getManager().takingOverProcess(cartel4.getName(), topTeam);
						} else {
							cartel4.setBeingTakenOver(false, null);
						}
					}
				} else {
					if (topTeam != cartel4.getTeam()) {
						CartelManager.getManager().takingOverProcess(cartel4.getName(), topTeam);
					}
				}
			}
		} else {
			if (cartel1.isBeingTakenOver()) {
				CartelManager.getManager().cancelTakingOver(cartel1);
			}
		}
		
		if (cartel5 != null) {
			Team topTeam = cartel5.getTopPlayerTeam();
			
			if (topTeam != null) {
				if (cartel5.isBeingTakenOver()) {
					if (topTeam != cartel5.getTeamTakingOver()) {
						if (topTeam != cartel5.getTeam()) {
							CartelManager.getManager().takingOverProcess(cartel5.getName(), topTeam);
						} else {
							cartel5.setBeingTakenOver(false, null);
						}
					}
				} else {
					if (topTeam != cartel5.getTeam()) {
						CartelManager.getManager().takingOverProcess(cartel5.getName(), topTeam);
					}
				}
			}
		} else {
			if (cartel1.isBeingTakenOver()) {
				CartelManager.getManager().cancelTakingOver(cartel1);
			}
		}
		updateHolograms();
		clear();
	}
	
	public boolean isInCartel(Location location){
		RegionManager regionManager = worldGuard.getRegionManager(location.getWorld());
		ApplicableRegionSet regions = regionManager.getApplicableRegions(location);
		List<String> cartels = Main.getInstance().getConfig().getStringList("cartels");
		
		for (ProtectedRegion region : regions) {
			for (String cartel : cartels) {
				if (region.getId().equalsIgnoreCase(cartel)) {
					return true;
				}
			}
		}
		return false;
	}
	
	public String getCartel(Location location) {
		RegionManager regionManager = worldGuard.getRegionManager(location.getWorld());
		ApplicableRegionSet regions = regionManager.getApplicableRegions(location);
		List<String> cartels = Main.getInstance().getConfig().getStringList("cartels");
		
		if (regions.size() == 0){
			return null;
		} else {
			for (ProtectedRegion region : regions){
				for (String cartel : cartels){
					if (region.getId().equalsIgnoreCase(cartel)) {
						return cartel;
					}
				}
			}
			return null;
		}
	}
	
	public void clear() {
		for (int i = 1; i < 6; i++) {
			Cartel cartel = CartelManager.getManager().getCartel("drug" + i);
			cartel.getTeamsAndCount().clear();
			
			cartel1Players.clear();
			cartel2Players.clear();
			cartel3Players.clear();
			cartel4Players.clear();
			cartel5Players.clear();
		}
	}
	
	public void updateHolograms() {
		for (Cartel cartel : Cartel.cartelObjects) {
			CartelManager.getManager().updateHolo(cartel.getName());
		}
	}
}
